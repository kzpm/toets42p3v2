!R1
en
conf t
h R1

int g0/0/2
ip add 192.168.4.1 255.255.252.0
no shut

int g0/0/1
ip add 10.10.10.1 255.255.255.252
no shut

int g0/0/0
ip add 10.40.10.1 255.255.255.252
no shut

exit

router ospf 1
router-id 8.8.8.8
network 192.168.4.0 0.0.3.255 area 0
network 10.10.10.0 0.0.0.3 area 0
network 10.40.10.0 0.0.0.3 area 0

end


!R2
en
conf t
h R2

int g0/0/2
ip add 192.168.8.1 255.255.254.0
no shut

int g0/0/0
ip add 10.30.10.1 255.255.255.252
no shut

int g0/0/1
ip add 10.40.10.2 255.255.255.252
no shut

exit 

router ospf 1
router-id 6.6.6.6
network 192.168.8.0 0.0.1.255 area 0
network 10.30.10.0 0.0.0.3 area 0
network 10.40.10.0 0.0.0.3 area 0


end



!R3
en
conf t
h R3

int g0/0/2
ip add 192.168.10.1 255.255.255.128
no shut

int g0/0/0
ip add 10.20.10.1 255.255.255.252
no shut

int g0/0/1
ip add 10.30.10.2 255.255.255.252
no shut

exit

router ospf 1
router-id 4.4.4.4
network 192.168.10.0 0.0.0.127 area 0
network 10.30.10.0 0.0.0.3 area 0
network 10.20.10.0 0.0.0.3 area 0

end

!R4
en
conf t
h R4

int g0/0/2
ip add 192.168.10.129 255.255.255.224
no shut

int g0/0/0
ip add 10.10.10.2 255.255.255.252
no shut

int g0/0/1
ip add 10.20.10.2 255.255.255.252
no shut

exit

router ospf 1
router-id 2.2.2.2
network 192.168.10.128 0.0.0.31 area 0
network 10.10.10.0 0.0.0.3 area 0
network 10.20.10.0 0.0.0.3 area 0

end

